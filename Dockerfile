FROM node:10-buster

USER 0:0
RUN groupadd -r -g 999 docker
RUN useradd -m -d /home/node/app -u 999 -g docker -s /bin/bash -r docker
RUN mkdir -p ./home/node/app/
WORKDIR /home/node/app/
ADD . /home/node/app
RUN chown -R 999.999 /home/node

USER 999:999
RUN yarn install
RUN yarn build
RUN rm -rf node_modules/*
RUN npm install express morgan express-session swig helmet cors errorhandler connect-history-api-fallback dotenv web3 truffle-contract qrcode abi-decoder axios png-to-jpeg nodemailer multer forever
USER 0:0
RUN npm install forever -g

ENTRYPOINT ["/usr/bin/env"]
CMD ["forever", "index.js"]
